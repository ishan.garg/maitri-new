import { StringConst } from '../../const/string-const';

export const NavTitles = [
    {
        title: 'Home',
        href: '#'
    },
    {
        title: 'Blog',
        href: 'https://blog.matrimilan.com'
    },
    {
        title: 'Contact Us',
        href: '#'
    },
    {
        title: 'Use App',
        href: StringConst.webLink
    }
];

export const CommunitySectionData = {
    title: 'We have people from different communities & locations.',
    subtitle1:
        'Our platform offers the opportunity to connect with individuals from different communities, religions, and cultures, allowing you to broaden your horizons and find that special someone who shares your values and beliefs.',
    subtitle2: 'Join us today and experience the joy of discovering your perfect match in a vibrant and inclusive community.',
    stats: [
        {
            count: 100000,
            title: 'Profiles from USA',
            users: [
                'https://media.istockphoto.com/id/1300972573/photo/pleasant-young-indian-woman-freelancer-consult-client-via-video-call.jpg?s=612x612&w=0&k=20&c=cbjgWR58DgUUETP6a0kpeiKTCxwJydyvXZXPeNTEOxg=',
                'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFsZSUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80',
                'https://media.istockphoto.com/id/1422746932/photo/indigenous-young-woman-smiling-while-looking-at-camera-outdoor-focus-on-face.jpg?b=1&s=170667a&w=0&k=20&c=jduJ1WGV8Me2_oa4AbjkJT2b2xm5OI8F1R981KeMcag=',
                'https://media.istockphoto.com/id/1309328823/photo/headshot-portrait-of-smiling-male-employee-in-office.jpg?b=1&s=170667a&w=0&k=20&c=MRMqc79PuLmQfxJ99fTfGqHL07EDHqHLWg0Tb4rPXQc=',
                'https://media.istockphoto.com/id/1303206558/photo/headshot-portrait-of-smiling-businessman-talk-on-video-call.jpg?s=612x612&w=0&k=20&c=hMJhVHKeTIznZgOKhtlPQEdZqb0lJ5Nekz1A9f8sPV8='
            ]
        },
        {
            count: 50000,
            title: 'Profiles from Europe and UK',
            users: [
                'https://media.istockphoto.com/id/1300972573/photo/pleasant-young-indian-woman-freelancer-consult-client-via-video-call.jpg?s=612x612&w=0&k=20&c=cbjgWR58DgUUETP6a0kpeiKTCxwJydyvXZXPeNTEOxg=',
                'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFsZSUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80',
                'https://media.istockphoto.com/id/1422746932/photo/indigenous-young-woman-smiling-while-looking-at-camera-outdoor-focus-on-face.jpg?b=1&s=170667a&w=0&k=20&c=jduJ1WGV8Me2_oa4AbjkJT2b2xm5OI8F1R981KeMcag=',
                'https://media.istockphoto.com/id/1309328823/photo/headshot-portrait-of-smiling-male-employee-in-office.jpg?b=1&s=170667a&w=0&k=20&c=MRMqc79PuLmQfxJ99fTfGqHL07EDHqHLWg0Tb4rPXQc=',
                'https://media.istockphoto.com/id/1303206558/photo/headshot-portrait-of-smiling-businessman-talk-on-video-call.jpg?s=612x612&w=0&k=20&c=hMJhVHKeTIznZgOKhtlPQEdZqb0lJ5Nekz1A9f8sPV8='
            ]
        },
        {
            count: 75000,
            title: 'Profiles from Middle east',
            users: [
                'https://media.istockphoto.com/id/1300972573/photo/pleasant-young-indian-woman-freelancer-consult-client-via-video-call.jpg?s=612x612&w=0&k=20&c=cbjgWR58DgUUETP6a0kpeiKTCxwJydyvXZXPeNTEOxg=',
                'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFsZSUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80',
                'https://media.istockphoto.com/id/1422746932/photo/indigenous-young-woman-smiling-while-looking-at-camera-outdoor-focus-on-face.jpg?b=1&s=170667a&w=0&k=20&c=jduJ1WGV8Me2_oa4AbjkJT2b2xm5OI8F1R981KeMcag=',
                'https://media.istockphoto.com/id/1309328823/photo/headshot-portrait-of-smiling-male-employee-in-office.jpg?b=1&s=170667a&w=0&k=20&c=MRMqc79PuLmQfxJ99fTfGqHL07EDHqHLWg0Tb4rPXQc=',
                'https://media.istockphoto.com/id/1303206558/photo/headshot-portrait-of-smiling-businessman-talk-on-video-call.jpg?s=612x612&w=0&k=20&c=hMJhVHKeTIznZgOKhtlPQEdZqb0lJ5Nekz1A9f8sPV8='
            ]
        }
    ]
};

export const TestimonialSectionData = {
    title: 'Testimonials',
    data: [
        {
            author_image: 'https://images.pexels.com/photos/938639/pexels-photo-938639.jpeg?cs=srgb&dl=pexels-ravi-k-938639.jpg&fm=jpg',
            author_name: 'Rohit Sindhu',
            message:
                "I had tried other matrimonial sites in the past, but none of them offered the level of personalization that Matrimilan did. From the moment I signed up, I felt like I was in good hands. The site's algorithm suggested matches based on my preferences, and I was able to connect with someone who shared my values and beliefs. We hit it off right away, and I knew I had found my life partner. Thanks, Matrimilan!"
        },
        {
            author_image:
                'https://media.istockphoto.com/id/522189179/photo/today-i-choose-to-be-happy.jpg?s=612x612&w=0&k=20&c=u_29xtihuEvWf-LkYaiyLd85wPh7uS2MrVa8dMwYNbk=',
            author_name: 'Shailendra Kumar',
            message:
                "As a busy professional, I didn't have a lot of time to devote to finding a partner. Matrimilan's user-friendly interface and efficient matchmaking process made it easy for me to find potential matches that fit my criteria. I was able to chat with multiple people before finding someone who really clicked with me. Matrimilan helped me find my soulmate, and I couldn't be happier with the results!"
        },
        {
            author_image: '/assets/image/home/testimonial-section/testimonials-image.png',
            author_name: 'Rimsha Naaz',
            message:
                "Matrimilan was a game-changer for me. I had tried other matrimonial sites in the past, but none of them came close to the quality of matches I found on this platform. I was able to connect with someone who shared my values and interests, and we're now planning our wedding. Thank you, Matrimilan, for helping me find my life partner!"
        }
    ]
};

export const BlogSectionData = {
    title: 'Matrimilan Blogs',
    blogs_data: [
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/strenghthen-love.jpg.webp',
            blog_title: 'Strengthening Love: Support Your Partner’s Mental Health',
            blog_message:
                'Relationships require effort and attention to thrive, and one of the most crucial elements of a healthy and fulfilling partnership is mutual support. Supporting your partner emotionally, physically, and mentally is essential for building a strong and lasting relationship.',
            blog_URL: 'https://blog.matrimilan.com/support-your-partners/',
            author_name: 'Trisha Iyer',
            author_image:
                'https://media.istockphoto.com/id/1300972573/photo/pleasant-young-indian-woman-freelancer-consult-client-via-video-call.jpg?s=612x612&w=0&k=20&c=cbjgWR58DgUUETP6a0kpeiKTCxwJydyvXZXPeNTEOxg=',
            blog_date: '21-04-2022'
        },
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/gender-equality-in-marriage.jpg',
            blog_title: 'Gender Equality in Marriages: Redefining Matrimony by Matrimilan',
            blog_message:
                'In today’s progressive world, breaking gender stereotypes is more important than ever. As we strive to create a society that values equality and inclusivity, we must challenge the traditional gender roles that have been ingrained in our culture, particularly in matrimonial relationships.',
            blog_URL: 'https://blog.matrimilan.com/gender-equality-in-marriages/',
            author_name: 'Rohit Patel',
            author_image:
                'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFsZSUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80',
            blog_date: '02-04-2023'
        },
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/mastering-compromise.jpg.webp',
            blog_title: 'Mastering Compromise: Finding Middle Ground in Marriages',
            blog_message:
                'Marriage is a beautiful institution that brings two people together, and it is supposed to be a source of joy, love, and companionship. However, it’s not uncommon for couples to find themselves at odds, struggling to navigate their differences and find common ground.',
            blog_URL: 'https://blog.matrimilan.com/finding-middle-ground-in-marriages/',
            author_name: 'Sanjana Rao',
            author_image:
                'https://media.istockphoto.com/id/1422746932/photo/indigenous-young-woman-smiling-while-looking-at-camera-outdoor-focus-on-face.jpg?b=1&s=170667a&w=0&k=20&c=jduJ1WGV8Me2_oa4AbjkJT2b2xm5OI8F1R981KeMcag=',
            blog_date: '21-01-2023'
        },
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/achieving-harmony.jpg.webp',
            blog_title: 'Achieving Harmony: Work-Life Balance for Married Couples',
            blog_message:
                'The importance of work-life balance cannot be overstated. In a world where we are constantly bombarded with work-related emails, notifications, and tasks, it can be difficult to switch off and focus on our personal lives.',
            blog_URL: 'https://blog.matrimilan.com/work-life-balance-for-married-couples/',
            author_name: 'Akash Shah',
            author_image: '/assets/image/home/blogs-section/blog-author.png',
            blog_date: '01-03-2023'
        },
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/The-Pillars-of-Indian-Matrimonial-Bliss.jpg',
            blog_title: 'Family, Culture, and Love: The Pillars of Indian Matrimonial Bliss',
            blog_message:
                'In many cultures, especially in India, the role of the family in matrimonial relationships is significant. Families participate in the matchmaking process and contribute to a marriage’s success and longevity.',
            blog_URL: 'https://blog.matrimilan.com/pillars-of-indian-matrimonial-bliss/',
            author_name: 'Khadija Siddiqui',
            author_image:
                'https://media.istockphoto.com/id/1309328823/photo/headshot-portrait-of-smiling-male-employee-in-office.jpg?b=1&s=170667a&w=0&k=20&c=MRMqc79PuLmQfxJ99fTfGqHL07EDHqHLWg0Tb4rPXQc=',
            blog_date: '07-06-2022'
        },
        {
            blog_image: 'https://blog.matrimilan.com/wp-content/uploads/2023/04/Inclusive-Role-of-Family-in-Matrimonial-Relationships-.jpg',
            blog_title: 'Inclusive Role of Family in Matrimonial Relationships',
            blog_message:
                'Matrimilan, as a matrimonial website and service provider, adopt various strategies to include families more inclusively in the matchmaking process and foster stronger matrimonial relationships.',
            blog_URL: 'https://blog.matrimilan.com/matrimonial-relationships/',
            author_name: 'Arif Ahmed',
            author_image:
                'https://media.istockphoto.com/id/1303206558/photo/headshot-portrait-of-smiling-businessman-talk-on-video-call.jpg?s=612x612&w=0&k=20&c=hMJhVHKeTIznZgOKhtlPQEdZqb0lJ5Nekz1A9f8sPV8=',
            blog_date: '04-04-2022'
        }
    ]
};

export const FooterSectionData = {
    title: 'Join Matrimilan today',
    subtitle: 'Begin your journey by taking the first step towards finding your perfect match with us.'
};

export const WhyYouShouldSection = {
    title: 'Reasons to join Matrimilan',
    desc: 'At Matrimilan, we understand how crucial it is to find the right match, and we are dedicated to making the process as seamless and stress-free as possible. Here are just a few of the reasons why you should join Matrimilan.'
};

export const CircleFeatureData = [
    {
        id: 1,
        title: '1500k+',
        subTitle: 'Verified profiles',
        desc: 'We take the safety and security of our members seriously, which is why we ensure that all profiles are verified to ensure the authenticity of the information provided',
        bg: '/assets/image/home/circle-features/ellipse-91.png'
    },
    {
        id: 2,
        title: 'Large',
        subTitle: 'user base',
        desc: 'We are proud to have a large and diverse user base of over 1.7 million individuals from all over the world. ',
        bg: '/assets/image/home/circle-features/ellipse-92.png'
    },
    {
        id: 3,
        title: 'Top-Notch',
        subTitle: ' Privacy',
        desc: 'We understand that privacy is important, which is why we offer advanced privacy features that allow you to control who sees your profile and contact information.',
        bg: '/assets/image/home/circle-features/ellipse-93.png'
    },
    {
        id: 4,
        title: '24*7',
        subTitle: 'customer support ',
        desc: 'Our customer support team is comprised of knowledgeable and friendly professionals who are dedicated to providing the best possible service to our users',
        bg: '/assets/image/home/circle-features/ellipse-94.png'
    }
];

export const AppSectionData = {
    title: 'Matrimilan',
    description:
        'We believe that everyone deserves to find love and companionship, and we are dedicated to helping you achieve that. Our advanced search algorithms and personalized matchmaking services help you to connect with individuals who are compatible with you in terms of values, interests, and lifestyle.\nJoin Matrimilan today and take the first step towards finding your perfect life partner..'
};

export const FeaturesSectionData = [
    {
        id: 1,
        img: '/assets/icons/home/features-section/section-3-hand.svg',
        title: 'Advanced algorithms',
        description:
            'We offer sophisticated algorithms that use machine learning and AI to match compatible partners based on a range of factors, including personality, values, interests, and lifestyle. This would potentially provide more accurate and successful matches compared to other matrimony platforms.'
    },
    {
        id: 2,
        img: '/assets/icons/home/features-section/section-3-hand.svg',
        title: 'Safety and Security',
        description:
            'We prioritize safety and security by implementing strict verification processes for users, including ID checks, background checks, and social media verification. This would help ensure that users are genuine and trustworthy, and minimize the risk of fraudulent or malicious activity on the platform.'
    },
    {
        id: 3,
        img: '/assets/icons/home/features-section/section-3-hand.svg',
        title: 'Customized services',
        description:
            'Matrimilan offers customized services such as personalized matchmaking, relationship coaching, and wedding planning. These services would help you find your ideal partner and navigate the process of getting married with greater ease and support.'
    }
];

export const EligibilitySectionData = {
    heading: 'Marriage made easy with Matrimilan',
    buttonText: 'Sign up today',
    list: [
        { text: 'We are committed to providing you with a gender-inclusive service' },
        { text: 'Our highly dedicated support team is available 24x7 to assist you' },
        { text: 'We provide a secure communication tool that prioritises your privacy' },
        { text: 'Through identity verification, we ensure safety and security of our users' },
        { text: 'We offer profile enhancement services to help you create a standout profile' }
    ]
};

export const EarlyAccessSectionData = {
    heading: 'Let Matrimilan help you find your perfect partner',
    paragraph:
        'At Matrimilan, we understand that marriage is a life-changing decision, and we are committed to helping you make the right choice. Our platform offers a range of features and tools, including chat and video call facilities, to enable you to get to know your potential partner better before taking the next step',
    buttonText: 'Search your partner now'
};

export const HeroSectionData = {
    title: 'Find you forever with Matrimilan',
    description:
        'We are an online matchmaking platform designed to help you find your perfect life partner. Our platform is built on trust, privacy, and security principles, ensuring that your search for a life partner is safe and successful',
    subHeading: 'You’re searching for...'
};
