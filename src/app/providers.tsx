"use client";

import React from "react";
import { ThemeProvider } from "styled-components";
import { MatriDefaultTheme } from "./theme/matri-default.theme";
import StyledComponentsRegistry from "./registry";

const AppProvider = ({ children }: { children: React.ReactNode }) => {
  return (
    <ThemeProvider theme={MatriDefaultTheme}>
      <StyledComponentsRegistry>{children}</StyledComponentsRegistry>
    </ThemeProvider>
  );
};

export default AppProvider;
