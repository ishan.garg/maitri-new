import { Paragraph } from "../../../../components/atoms/title-atom";
import { DeviceSize } from "../../../../const/device-size-const";
import { FC } from "react";
import styled from "styled-components";

const StyledComponent = {
  CircleOuterContainer: styled.div`
        width: 50%;
        align-items: center;
        display: flex;
        flex-direction: column;

        @media screen and (max-width: ${DeviceSize.mobileL}) {
            width: 100%;
        }
    `,
  MainTitle: styled.div`
        width: 100%;
        text-align: center;
        position: absolute;
        top: 40%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-weight: 700;
        font-size: 2rem;
        letter-spacing: -0.01em;
        color: #ffffff;

        @media screen and (max-width: ${DeviceSize.laptop}) {
            font-size: 2.25rem;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            font-size: 1.5rem;
        }
    `,
  SubTitle: styled.div`
        position: absolute;
        top: 55%;
        left: 50%;
        width: 100%;
        text-align: center;
        transform: translate(-50%, -50%);
        font-weight: 400;
        font-size: 1.3rem;
        letter-spacing: -0.01em;
        color: #ffffff;
        @media screen and (max-width: ${DeviceSize.laptop}) {
            font-size: 1.5rem;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            font-size: 0.938rem;
        }
    `,
};

interface MyProps {
  bg: string;
  title: string;
  subTitle: string;
  desc: string;
}

export const CircleFeature: FC<MyProps> = ({ bg, title, subTitle, desc }) => {
  const FeatureCircle = styled.div`
        width: 100%;
        max-width: 220px;
        aspect-ratio: 1/1;
        margin: 10px 20px;
        border-radius: 50%;
        background: radial-gradient(0, rgba(46, 40, 53, 0.7), rgba(46, 40, 53, 0.7)), url(${bg});
        background-position: center;
        position: relative;

        @media screen and (max-width: ${DeviceSize.tabletS}) {
            max-width: 200px;
        }

        @media screen and (max-width: ${DeviceSize.mobileL}) {
            max-width: 180px;
        }
    `;

  return (
    <StyledComponent.CircleOuterContainer>
      <FeatureCircle>
        <StyledComponent.MainTitle>{title}</StyledComponent.MainTitle>
        <StyledComponent.SubTitle>{subTitle}</StyledComponent.SubTitle>
      </FeatureCircle>
      <Paragraph color="#2E2835" textalign="center">
        {desc}
      </Paragraph>
    </StyledComponent.CircleOuterContainer>
  );
};
