import { Paragraph } from "../../../../components/atoms/title-atom";
import { DeviceSize } from "../../../../const/device-size-const";
import Image from "next/image";
import { FC } from "react";
import styled from "styled-components";

interface MyProps {
  key: number;
  img: string;
  title: string;
  description: string;
}

const StyledComponent = {
  Feature: styled.div`
        width: 30%;
        display: flex;
        flex-direction: column;
        align-items: center;
        margin: 0.625rem 1.25rem 3.125rem;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            min-width: 95%;
            margin: 0.625rem 0.625rem 3.125rem;
        }

        .feature-title {
            font-weight: 600;
            font-size: 1.375rem;
            letter-spacing: -0.01em;
            line-height: 1.75rem;
            text-align: center;
            color: #2e2835;
            margin: 1.25rem 0 0.6rem;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                font-size: 0.938rem;
            }
        }

        .feature-image-container {
            border-radius: 50%;
            width: 7.25rem;
            height: 7.25rem;
            background-color: rgba(128, 65, 198, 0.04);
            padding: 1.75rem;

            > div {
                position: unset !important;
            }

            .feature-image {
                object-fit: contain;
                width: 100% !important;
                position: relative !important;
                height: unset !important;
            }
        }
    `,
};

export const Feature: FC<MyProps> = ({ img, title, description }) => {
  return (
    <StyledComponent.Feature>
      <div className="feature-image-container">
        <Image className="feature-image" fill src={img} alt="feature" />
      </div>
      <div className="feature-title">{title}</div>
      <Paragraph color="#2e2835" textalign="center">
        {description}
      </Paragraph>
    </StyledComponent.Feature>
  );
};
