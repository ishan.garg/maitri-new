import { Paragraph } from "../../../../components/atoms/title-atom";
import { DeviceSize } from "../../../../const/device-size-const";
import Image from "next/image";
import { FC } from "react";
import styled from "styled-components";

interface TIProps {
  author_image: string;
  author_name: string;
  message: string;
}

export const TestimonialItem: FC<TIProps> = ({
  author_image,
  author_name,
  message,
}) => {
  return (
    <StyledComponent.Container>
      <StyledComponent.AuthorImage>
        <Image src={author_image} alt="testimonial author picture" fill />
      </StyledComponent.AuthorImage>
      <StyledComponent.Message>{message}</StyledComponent.Message>
      <StyledComponent.AuthorName fontWeight={600} fontSize="1.5rem">
        {author_name}
      </StyledComponent.AuthorName>
      <StyledComponent.VectorContainer>
        <Image
          src="/assets/vectors/home/testimonial-section/testimonial-vector-2.svg"
          alt="vector"
          width={40.55}
          height={80.99}
          className="Vector"
        />
        <Image
          src="/assets/vectors/home/testimonial-section/testimonial-vector-2.svg"
          alt="vector"
          width={40.55}
          height={80.99}
          className="Vector"
        />
      </StyledComponent.VectorContainer>
    </StyledComponent.Container>
  );
};

const StyledComponent = {
  Container: styled.div`
        display: flex;
        flex-direction: column;
        gap: 2rem;
        @media (max-width: ${DeviceSize.laptop}) {
            align-items: center;
            gap: 1.5rem;
        }

        .Vector {
            opacity: 0.1;
        }
    `,
  Message: styled(Paragraph)`
        @media (max-width: ${DeviceSize.laptop}) {
            text-align: center;
        }
    `,
  AuthorImage: styled.div`
        @media (min-width: ${DeviceSize.laptop}) {
            display: none;
        }
        @media (max-width: ${DeviceSize.laptop}) {
            position: relative;
            width: 10rem;
            height: 13.75rem;
            margin-top: -9.375rem;
        }

        > img {
            display: block;
            border-radius: 1.25rem 1.25rem 0 1.25rem;
            object-fit: cover;
        }
    `,
  AuthorName: styled(Paragraph)`
        @media (max-width: ${DeviceSize.tabletS}) {
            font-size: 1.25rem;
            text-align: center;
        }
    `,
  VectorContainer: styled.div`
        display: flex;
        align-items: center;
        gap: 0.938rem;
        position: absolute;
        bottom: 0;
        right: 0;
    `,
};
