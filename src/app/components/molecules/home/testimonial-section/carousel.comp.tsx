"use client";

import { FC } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper/modules";
import "swiper/css";
import { TestimonialItem } from "./testimonial-item.comp";
import styled from "styled-components";
import { DeviceSize } from "../../../../const/device-size-const";
import { TestimonialSectionData } from "../../../../data/home/home.data";

interface CCProps {
  swiperRef: any;
}

export const CarouselComponent: FC<CCProps> = ({ swiperRef }) => {
  return (
    <StyledComponent.BackgroundContainer>
      <StyledComponent.SwiperWrapper
        spaceBetween={50}
        slidesPerView={1}
        modules={[Navigation]}
        onBeforeInit={(swiper) => {
          swiperRef.current = swiper;
        }}
      >
        {TestimonialSectionData.data.map((item, index) => {
          return (
            <SwiperSlide key={index}>
              <TestimonialItem {...item} />
            </SwiperSlide>
          );
        })}
      </StyledComponent.SwiperWrapper>
    </StyledComponent.BackgroundContainer>
  );
};

const StyledComponent = {
  BackgroundContainer: styled.div`
        background: ${(props) => props.theme.color.primaryDark};
        flex: 1;
        border-radius: 0 0.938rem 0.938rem 0;
        color: white;
        padding: 2.7rem;
        box-sizing: border-box;
        max-width: inherit;
        @media (max-width: ${DeviceSize.laptop}) {
            border-radius: 0.938rem;
            padding: 1.25rem;
        }
    `,
  SwiperWrapper: styled(Swiper)`
        height: 100%;
        @media (max-width: ${DeviceSize.laptop}) {
            overflow: visible;
        }
    `,
};
