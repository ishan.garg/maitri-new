import {
  Paragraph,
  TitlePrimary,
} from "../../../../components/atoms/title-atom";
import { DeviceSize } from "../../../../const/device-size-const";
import Image from "next/image";
import { FC } from "react";
import styled from "styled-components";

interface SCProps {
  count: number;
  title: string;
  users: string[];
}

export const StatsCard: FC<SCProps> = ({ count, title, users }) => {
  return (
    <StyledComponent.Container>
      <StyledComponent.ImageContainer>
        {users.map((item, index) => {
          return (
            <StyledComponent.ImageWrapper key={index}>
              <Image src={item} alt="user" fill />
            </StyledComponent.ImageWrapper>
          );
        })}
      </StyledComponent.ImageContainer>
      <StyledComponent.Count>{count}+</StyledComponent.Count>
      <StyledComponent.Title>{title}</StyledComponent.Title>
    </StyledComponent.Container>
  );
};

const StyledComponent = {
  Container: styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        background: rgba(255, 255, 255, 0.9);
        box-shadow: 0rem 0.25rem 1.25rem rgba(128, 65, 198, 0.1);
        border-radius: 1.25rem;
        padding: 0.625rem 1rem;
        gap: 1.563rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            gap: 0.125rem;
        }
    `,
  ImageContainer: styled.div`
        display: flex;
        align-items: center;
        justify-content: center;
    `,
  ImageWrapper: styled.div`
        position: relative;
        width: 2.625rem;
        height: 2.625rem;
        display: block;

        &:not(:first-child) {
            margin-left: -0.938rem;
        }

        > img {
            border: 0.125rem solid white;
            object-fit: cover;
            border-radius: 100%;
        }
    `,
  Count: styled(TitlePrimary)`
        font-weight: 700;
        color: ${(props) => props.theme.color.purpleColor};
        font-size: 1.25rem;
        line-height: 0;
        @media (max-width: ${DeviceSize.tabletS}) {
            line-height: 2;
            font-size: 1.125rem;
        }
    `,
  Title: styled(Paragraph)`
        font-weight: 400;
        font-size: 0.938rem;
        margin-bottom: 0.813rem;
        color: ${(props) => props.theme.color.primaryDark};
        line-height: 0;
        @media (max-width: ${DeviceSize.tabletS}) {
            line-height: 1;
            font-size: 0.75rem;
        }
    `,
};
