import { Paragraph } from "../../../../components/atoms/title-atom";
import { FC } from "react";
import styled from "styled-components";
import { DeviceSize } from "../../../../const/device-size-const";
import Image from "next/image";

interface BIProps {
  blog_image: string;
  author_name: string;
  author_image: string;
  blog_title: string;
  blog_message: string;
  blog_date: string;
  blog_URL: string;
}

export const BlogItem: FC<BIProps> = ({
  blog_image,
  author_name,
  author_image,
  blog_title,
  blog_message,
  blog_date,
  blog_URL,
}) => {
  return (
    <StyledComponent.BlogContainer>
      <StyledComponent.BlogImageWrapper>
        <Image src={blog_image} alt="user blog picture" fill />
      </StyledComponent.BlogImageWrapper>
      <a href={blog_URL} target="__blank">
        <StyledComponent.Title fontWeight={600}>
          {blog_title}
        </StyledComponent.Title>
      </a>
      <StyledComponent.Message>{blog_message}</StyledComponent.Message>
      <StyledComponent.FooterContainer>
        <StyledComponent.AuthorContainer>
          <StyledComponent.AuthorImage>
            <Image src={author_image} alt="author picture" fill />
          </StyledComponent.AuthorImage>
          <div>
            <StyledComponent.AuthorName fontWeight={600}>
              {author_name}
            </StyledComponent.AuthorName>
            <StyledComponent.DateMobileView color="grey">
              Posted On: {blog_date}
            </StyledComponent.DateMobileView>
          </div>
        </StyledComponent.AuthorContainer>
        <StyledComponent.DateWebView
          fontWeight={600}
          fontSize="1rem"
          color="grey"
        >
          {blog_date}
        </StyledComponent.DateWebView>
      </StyledComponent.FooterContainer>
    </StyledComponent.BlogContainer>
  );
};

const StyledComponent = {
  BlogContainer: styled.div`
        display: flex;
        flex-direction: column;
        gap: 1rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            gap: 0.5rem;
        }
        color: ${(props) => props.theme.color.primaryDark};
    `,
  BlogImageWrapper: styled.div`
        position: relative;
        width: 100%;
        height: 12.5rem;

        > img {
            border-radius: 1.25rem;
            object-fit: cover;
        }
    `,
  Title: styled(Paragraph)`
        margin-top: 1rem;
    `,
  Message: styled(Paragraph)`
        text-overflow: ellipsis;
        overflow: hidden;
        display: -webkit-box !important;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        white-space: normal;
    `,
  FooterContainer: styled.div`
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin-top: 1rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            margin-top: 0.5rem;
        }
    `,
  AuthorContainer: styled.div`
        display: flex;
        align-items: center;
        gap: 1rem;
    `,
  AuthorImage: styled.div`
        position: relative;
        width: 3rem;
        height: 3rem;

        > img {
            border-radius: 100%;
            border: 2px solid #d5c4e8;
            object-fit: cover;
        }
    `,
  AuthorName: styled(Paragraph)`
        @media (max-width: ${DeviceSize.tabletS}) {
            font-size: 0.938rem;
        }
    `,
  DateWebView: styled(Paragraph)`
        @media (max-width: ${DeviceSize.tabletS}) {
            display: none;
        }
    `,
  DateMobileView: styled(Paragraph)`
        display: none;
        @media (max-width: ${DeviceSize.tabletS}) {
            display: block;
        }
    `,
};
