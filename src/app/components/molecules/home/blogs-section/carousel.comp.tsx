import { FC } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper/modules";
import "swiper/css";
import { BlogItem } from "./blog-item.comp";
import { BlogSectionData } from "../../../../data/home/home.data";
import { DeviceSize } from "../../../../const/device-size-const";

interface CCProps {
  swiperRef: any;
}

export const CarouselComponent: FC<CCProps> = ({ swiperRef }) => {
  return (
    <Swiper
      spaceBetween={60}
      slidesPerView={3}
      modules={[Navigation]}
      onBeforeInit={(swiper) => {
        swiperRef.current = swiper;
      }}
      breakpoints={{
        0: {
          slidesPerView: 1,
        },
        [parseFloat(DeviceSize.tabletS.slice(0, -2))]: {
          slidesPerView: 2,
        },
        [parseFloat(DeviceSize.laptop.slice(0, -2))]: {
          slidesPerView: 3,
        },
      }}
    >
      {BlogSectionData.blogs_data.map((item, index) => {
        return (
          <SwiperSlide key={index}>
            <BlogItem {...item} />
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};
