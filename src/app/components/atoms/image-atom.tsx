import { DeviceSize } from "../../const/device-size-const";
import { FC } from "react";
import styled, { css } from "styled-components";

interface IWProps {
  image: string;
  width?: string;
  height?: string;
  borderRadius?: string;
  absolute?: boolean;
  top?: string;
  bottom?: string;
  left?: string;
  right?: string;
  border?: string;
  widthMobile?: string;
}

export const ImageWrapper: FC<IWProps> = ({
  image,
  borderRadius,
  width,
  height,
  absolute,
  top,
  bottom,
  left,
  right,
  border,
  widthMobile,
}) => {
  return (
    <StyledComponent.ImageStyled
      src={image}
      borderRadius={borderRadius}
      width={width}
      height={height}
      absolute={absolute}
      top={top}
      bottom={bottom}
      left={left}
      right={right}
      border={border}
      widthMobile={widthMobile}
      alt="icon"
    />
  );
};

const StyledComponent = {
  ImageStyled: styled.img<{
    borderRadius?: string;
    width?: string;
    height?: string;
    absolute?: boolean;
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
    border?: string;
    widthMobile?: string;
  }>`
        width: ${(props) => props.width};
        height: ${(props) => props.height};
        border-radius: ${(props) => props.borderRadius};
        object-fit: cover;
        border: ${(props) => props.border};

        @media (max-width: ${DeviceSize.tabletS}) {
            width: ${(props) => props.widthMobile};
        }

        ${(props) =>
          props.absolute &&
          css`
                position: absolute;
                top: ${props.top};
                bottom: ${props.bottom};
                left: ${props.left};
                right: ${props.right};
            `}
    `,
};
