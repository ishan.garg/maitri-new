import { DeviceSize } from "../../const/device-size-const";
import styled from "styled-components";

export const WrapperComponent = styled.section`
    width: 100%;
    position: relative;
    padding: 50px 0;
    overflow: hidden;
    @media (max-width: ${DeviceSize.tabletS}) {
        padding: 20px 0;
    }
`;

export const MaxWidthComponent = styled.div`
    width: 100%;
    margin: 0 auto;
    max-width: 1024px;
    position: relative;
`;
