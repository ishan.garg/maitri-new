import styled from "styled-components";
import { DeviceSize } from "../../const/device-size-const";

interface IGetStartedButton {
  bgColor?: string;
}

interface IGCircleButton {
  width?: string;
  height?: string;
  background?: string;
  opacity?: number;
}

export const GetStartedButtonStyled = styled("button")<IGetStartedButton>`
    border-radius: 24px;
    background-color: ${(props) => (props.bgColor ? props.bgColor : "#000000")};
    height: 2.75rem;
    width: 13.375rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
`;

export const ReadMoreButtonStyled = styled.button`
    cursor: pointer;
    color: #0091ff;
    background-color: transparent;
    border: none;
    font-size: 1rem;
    margin: 0;
    padding: 0;
`;

const BasicButtonStyled = styled.button`
    font-size: 1rem;
    margin: 1rem;
    padding: 0.25em 1rem;
    background: gray;
    border-radius: 3px;
`;

const RoundedButtonStyled = styled(BasicButtonStyled)`
    border-radius: 0.6rem;
    background: lightgrey;
`;

const ActiveInactiveButtonStyled = styled(RoundedButtonStyled)<{
  active?: boolean;
}>`
    background: ${(props) => (props.active ? "green" : "orange")};
`;

const ThemeColoredButtonStyled = styled(BasicButtonStyled)`
    background: ${(props) => props.theme.color.primaryDark};
`;

const CircleButtonStyled = styled.button<IGCircleButton>`
    width: ${(props) => (props.width ? props.width : "2.7rem")};
    height: ${(props) => (props.height ? props.height : "2.7rem")};
    background: ${(props) => {
      if (props.background) {
        if (props.background === "purple") {
          return props.theme.color.purpleColor;
        } else if (props.background === "black") {
          return props.theme.color.primaryDark;
        } else {
          return props.background;
        }
      } else {
        return "transparent";
      }
    }};
    opacity: ${(props) => (props.opacity ? props.opacity : 1)};
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 100%;
    border: none;
    cursor: pointer;
    @media (max-width: ${DeviceSize.tabletS}) {
        width: 2rem;
        height: 2rem;
    }
`;

const ContainerButton = styled.button`
    border-radius: 80px;
    border: none;
    color: #8041c6;
    background-color: white;
    width: 30%;
    height: 3.125rem;
    font-weight: 600;
    margin-top: 1.875rem;
    font-size: 1.125rem;
    margin-bottom: 1rem;
    @media screen and (max-width: ${DeviceSize.tabletS}) {
        /* width: 17.5rem; */
        width: 50%;
        font-size: 1.125rem;
    }
    @media screen and (max-width: ${DeviceSize.mobileL}) {
        height: 2.813rem;
        padding: 0px;
        margin-top: 1.5rem;
        width: 60%;
        font-size: 0.75rem;
    }
    @media screen and (max-width: ${DeviceSize.mobileS}) {
        height: 2.5rem;
        /* width: 10.625rem; */
        font-size: 0.625rem;
    }
`;
export {
  ContainerButton,
  BasicButtonStyled,
  RoundedButtonStyled,
  ThemeColoredButtonStyled,
  ActiveInactiveButtonStyled,
  CircleButtonStyled,
};
