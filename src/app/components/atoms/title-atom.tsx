import { DeviceSize } from "../../const/device-size-const";
import styled from "styled-components";

interface IGParagraph {
  fontWeight?: number;
  fontSize?: string;
  color?: string;
  lineHeight?: number;
  width?: string;
  margintop?: string;
  textalign?: string;
}

interface PropsTitle {
  color?: string;
  textalign?: string;
  tabletFontSize?: string;
  lineHeight?: string;
}

export const TitlePrimary = styled.h1<PropsTitle>`
    font-weight: 600;
    font-size: 2.6rem;
    letter-spacing: -0.01em;
    color: ${(props) => props.color};
    text-align: ${(props) => (props.textalign ? props.textalign : "left")};
    line-height: ${(props) => (props.lineHeight ? props.lineHeight : "3.4rem")};

    @media (max-width: ${DeviceSize.tabletS}) {
        font-size: 1.063rem;
        line-height: 1.339rem;
        font-size: ${(props) =>
          props.tabletFontSize ? props.tabletFontSize : "1.063rem"};
    }
`;

export const Paragraph = styled.p<IGParagraph>`
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    font-size: ${(props) => (props.fontSize ? props.fontSize : "1.2rem")};
    letter-spacing: -0.01em;
    color: ${(props) => {
      if (props.color) {
        if (props.color === "grey") {
          return props.theme.color.blogDateColor;
        } else {
          return props.color;
        }
      }
    }};
    line-height: ${(props) => (props.lineHeight ? props.lineHeight : "2rem")};
    width: ${(props) => (props.width ? props.width : "")};
    margin-top: ${(props) => (props.margintop ? props.margintop : "")};
    text-align: ${(props) => (props.textalign ? props.textalign : "left")};
    @media (max-width: ${DeviceSize.tabletS}) {
        font-size: 0.75rem;
        line-height: 1.3rem;
    }
`;
