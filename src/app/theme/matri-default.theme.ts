import styled, { DefaultTheme } from 'styled-components';

export const MatriDefaultTheme: DefaultTheme = {
    borderRadius: '5px',
    color: {
        primaryDark: '#2E2835',
        primary: '#6c757d',
        primaryLight: '#f8f9fa',
        primaryAccent: '#28a745',
        inactiveButtonColor: '#6c757d',
        purpleColor: '#8041C6',
        blogDateColor: '#7D7D7D',
        testimonialBackgroundColor: '#2E283505'
    }
};

export const MaxWidthDiv = styled.div`
    max-width: 1272px;
    margin: 0 auto;
    padding: 0 16px;
`;
