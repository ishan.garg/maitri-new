export const DeviceSize = {
    mobileL: '425px',
    mobileS: '320px',
    tabletS: '768px',
    laptop: '1024px'
};
