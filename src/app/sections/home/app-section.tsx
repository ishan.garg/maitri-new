"use client";

import { Paragraph, TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { DeviceSize } from "../../const/device-size-const";
import { FC } from "react";
import styled from "styled-components";
import Image from "next/image";
import { StringConst } from "../../const/string-const";

interface MyProps {
  title: string;
  description: string;
}

const StyledComponent = {
  ResponsiveFlexContainer: styled.div`
        display: flex;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            flex-direction: column;
        }
    `,
  ColouredWrapper: styled(WrapperComponent)`
        background-color: rgba(46, 40, 53, 0.06);

        .responsive-flex-container {
            display: flex;

            @media screen and (max-width: ${DeviceSize.tabletS}) {
                flex-direction: column;
            }

            .left-box {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                padding: 1.25rem;
                flex: 1;

                .app-image-container {
                    width: 90%;
                    max-width: 500px !important;
                    margin: 1.25rem 0 0;
                    z-index: 2;

                    > div {
                        position: unset !important;
                    }

                    .app-image {
                        object-fit: contain;
                        width: 100% !important;
                        position: relative !important;
                        height: unset !important;
                    }
                }

                .left-bg-vector {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: 95%;
                    max-width: 39.375rem;
                    z-index: 1;
                }
            }

            .right-box {
                padding: 1.25rem;
                flex: 1;

                @media screen and (max-width: ${DeviceSize.tabletS}) {
                    padding-top: 0;
                }

                .purple-tag {
                    margin: 3.75rem 0 1.25rem;
                    font-weight: 600;
                    font-size: 1.125rem;
                    line-height: 4rem;
                    color: #8041c6;
                    letter-spacing: -0.01em;

                    @media screen and (max-width: ${DeviceSize.tabletS}) {
                        font-size: 0.75rem;
                    }
                }

                .stores-image-container {
                    display: flex;
                    width: 100%;
                    max-width: 18.375rem;
                    gap: 2.063rem;

                    > a {
                        width: 50%;

                        .store-icon {
                            object-fit: contain;
                            width: 100% !important;
                            position: relative !important;
                            height: unset !important;
                        }
                    }
                }

                .right-upper-vector {
                    position: absolute;
                    right: 5%;
                    top: 5%;
                }

                .right-lower-vector {
                    position: absolute;
                    right: 0;
                    bottom: 3%;
                }
            }
        }
    `,
  RightTitle: styled(TitlePrimary)`
        margin: 4.375rem 0 1.25rem;
        color: #2e2835;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            margin: 2.5rem 0 1.25rem;
        }
    `,
};

export const AppSection: FC<MyProps> = ({ title, description }) => {
  return (
    <StyledComponent.ColouredWrapper>
      <MaxWidthDiv>
        <div className="responsive-flex-container">
          <div className="left-box">
            <div className="app-image-container">
              <Image
                fill
                className="app-image"
                src="/assets/image/home/app-section/mobile.png"
                alt="mobile"
              />
            </div>
            <Image
              width={589}
              height={447}
              className="left-bg-vector"
              src="/assets/vectors/home/app-section/section-4-left.svg"
              alt=""
            />
          </div>

          <div className="right-box">
            <Image
              width={68}
              height={38}
              className="right-upper-vector"
              src="/assets/vectors/home/app-section/app-section-upper.svg"
              alt=""
            />
            <Image
              width={93}
              height={107}
              className="right-lower-vector"
              src="/assets/vectors/home/app-section/app-section-lower.svg"
              alt=""
            />

            <StyledComponent.RightTitle>{title}</StyledComponent.RightTitle>
            <Paragraph color="#2e2835">
              <span> {description.split("\n")[0]}</span> <br /> <br />{" "}
              <span>{description.split("\n")[1]}</span>{" "}
            </Paragraph>

            <a href={StringConst.webLink} className="purple-tag">
              Use the web app now
            </a>
            <div className="stores-image-container">
              <a href="https://play.google.com/store/apps">
                <Image
                  fill
                  className="store-icon"
                  src="/assets/image/home/app-section/playstore.svg"
                  alt="playstore icon"
                />
              </a>
              <a href="https://www.apple.com/in/app-store/">
                <Image
                  fill
                  className="store-icon"
                  src="/assets/image/home/app-section/appstore.svg"
                  alt="appstore icon"
                />
              </a>
            </div>
          </div>
        </div>
      </MaxWidthDiv>
    </StyledComponent.ColouredWrapper>
  );
};
