"use client";

import { TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { Feature } from "../../components/molecules/home/features-section/feature-molecule";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { DeviceSize } from "../../const/device-size-const";
import { FC } from "react";
import styled from "styled-components";
import { FeaturesSectionData } from "../../data/home/home.data";
import Image from "next/image";

interface MyObject {
  id: number;
  img: string;
  title: string;
  description: string;
}

const ModifiedWrapper = styled(WrapperComponent)`
    padding: 0;

    .feature-container {
        display: flex;
        justify-content: space-between;
        &::-webkit-scrollbar {
            display: none;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            overflow-x: auto;
        }
    }

    .left-blob {
        position: absolute;
        top: 3%;
        left: 0%;
        @media screen and (max-width: ${DeviceSize.laptop}) {
            display: none;
        }
    }

    .right-blob {
        position: absolute;
        top: 1%;
        right: 0%;

        @media screen and (max-width: ${DeviceSize.laptop}) {
            width: 3.125rem !important;
            height: unset !important;
            top: 40%;
        }
    }
`;

export const FeaturesSection: FC = () => {
  return (
    <ModifiedWrapper>
      <MaxWidthDiv>
        <Image
          width={249}
          height={209}
          className="left-blob"
          alt=""
          src="/assets/vectors/home/features-section/section-3-blob-left.svg"
        />
        <Image
          width={139}
          height={159}
          className="right-blob"
          alt=""
          src="/assets/vectors/home/features-section/section-blob-right.svg"
        />
        <TitlePrimary color="#2c2c2c">What makes us different?</TitlePrimary>
        <div className="feature-container">
          {FeaturesSectionData.map((item: MyObject) => (
            <Feature key={item.id} {...item} />
          ))}
        </div>
      </MaxWidthDiv>
    </ModifiedWrapper>
  );
};
