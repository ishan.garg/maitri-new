"use client";

import { TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { CircleButtonStyled } from "../../components/atoms/button-atom";
import { CarouselComponent } from "../../components/molecules/home/blogs-section/carousel.comp";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { FC, useRef } from "react";
import styled from "styled-components";
import { Swiper as SwiperType } from "swiper";
import { BlogSectionData } from "../../data/home/home.data";
import { DeviceSize } from "../../const/device-size-const";
import Image from "next/image";

export const BlogsSection: FC = () => {
  const swiperRef = useRef<SwiperType>();
  const prevSlide = () => {
    swiperRef.current?.slidePrev();
  };
  const nextSlide = () => {
    swiperRef.current?.slideNext();
  };
  return (
    <WrapperComponent>
      <MaxWidthDiv>
        <StyledComponent.HeaderContainer>
          <StyledComponent.Title>{BlogSectionData.title}</StyledComponent.Title>
          <div>
            <CircleButtonStyled
              background="black"
              width="3.2rem"
              height="3.2rem"
              onClick={prevSlide}
            >
              <StyledComponent.ArrowIcon>
                <Image
                  src="/assets/icons/home/blogs-section/left-arrow.svg"
                  alt="arrow"
                  fill
                />
              </StyledComponent.ArrowIcon>
            </CircleButtonStyled>
            <CircleButtonStyled
              background="black"
              width="3.2rem"
              height="3.2rem"
              onClick={nextSlide}
            >
              <StyledComponent.ArrowIcon>
                <Image
                  src="/assets/icons/home/blogs-section/right-arrow.svg"
                  alt="arrow"
                  fill
                />
              </StyledComponent.ArrowIcon>
            </CircleButtonStyled>
          </div>
        </StyledComponent.HeaderContainer>
        <CarouselComponent swiperRef={swiperRef} />
      </MaxWidthDiv>
    </WrapperComponent>
  );
};

const StyledComponent = {
  HeaderContainer: styled.div`
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 2.5rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            margin-bottom: 1.25rem;
        }

        > div {
            display: flex;
            align-items: center;
            gap: 1.4rem;
        }
    `,
  Title: styled(TitlePrimary)`
        color: ${(props) => props.theme.color.primaryDark};
    `,
  ArrowIcon: styled.div`
        position: relative;
        width: 0.938rem;
        height: 0.938rem;
        > img {
            object-fit: cover;
        }
        @media (max-width: ${DeviceSize.tabletS}) {
            width: 0.625rem;
            height: 0.625rem;
        }
    `,
};
