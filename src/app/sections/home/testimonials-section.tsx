"use client";

import { TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { FC, useRef, useState, useEffect } from "react";
import styled from "styled-components";
import { Swiper } from "swiper";
import { ImageWrapper } from "../../components/atoms/image-atom";
import { CircleButtonStyled } from "../../components/atoms/button-atom";
import { CarouselComponent } from "../../components/molecules/home/testimonial-section/carousel.comp";
import { TestimonialSectionData } from "../../data/home/home.data";
import { DeviceSize } from "../../const/device-size-const";
import Image from "next/image";

export const TestimonialSection: FC = () => {
  const swiperRef = useRef<Swiper>();
  const [activeSlideIndex, setActiveSlideIndex] = useState<any>(0);
  const prevSlide = () => {
    swiperRef.current?.slidePrev();
    setActiveSlideIndex(swiperRef.current?.activeIndex);
  };
  const nextSlide = () => {
    swiperRef.current?.slideNext();
    setActiveSlideIndex(swiperRef.current?.activeIndex);
  };
  useEffect(() => {
    if (swiperRef.current) {
      setActiveSlideIndex(swiperRef.current?.activeIndex);
    }
  }, [swiperRef]);
  return (
    <StyledComponent.WrapperContainer>
      <StyledComponent.MaxWidthWrapper>
        <StyledComponent.AuthorImage>
          <Image
            src={TestimonialSectionData.data[activeSlideIndex].author_image}
            alt="testimonial author picture"
            fill
          />
        </StyledComponent.AuthorImage>
        <StyledComponent.MainContainer>
          <StyledComponent.HeaderComponent>
            <StyledComponent.Title>
              {TestimonialSectionData.title}
            </StyledComponent.Title>
            <StyledComponent.IconsGroup>
              <CircleButtonStyled
                background="purple"
                width="3.2rem"
                height="3.2rem"
                onClick={prevSlide}
              >
                <StyledComponent.ArrowIcon>
                  <Image
                    src="/assets/icons/home/blogs-section/left-arrow.svg"
                    alt="arrow"
                    fill
                  />
                </StyledComponent.ArrowIcon>
              </CircleButtonStyled>
              <CircleButtonStyled
                background="purple"
                width="3.2rem"
                height="3.2rem"
                onClick={nextSlide}
              >
                <StyledComponent.ArrowIcon>
                  <Image
                    src="/assets/icons/home/blogs-section/right-arrow.svg"
                    alt="arrow"
                    fill
                  />
                </StyledComponent.ArrowIcon>
              </CircleButtonStyled>
            </StyledComponent.IconsGroup>
          </StyledComponent.HeaderComponent>
          <CarouselComponent swiperRef={swiperRef} />
        </StyledComponent.MainContainer>
        <StyledComponent.IconsGroupMobileView>
          <CircleButtonStyled
            background="purple"
            width="3.625rem"
            height="3.625rem"
            onClick={prevSlide}
          >
            <StyledComponent.ArrowIcon>
              <Image
                src="/assets/icons/home/blogs-section/left-arrow.svg"
                alt="arrow"
                fill
              />
            </StyledComponent.ArrowIcon>
          </CircleButtonStyled>
          <CircleButtonStyled
            background="purple"
            width="3.625rem"
            height="3.625rem"
            onClick={nextSlide}
          >
            <StyledComponent.ArrowIcon>
              <Image
                src="/assets/icons/home/blogs-section/right-arrow.svg"
                alt="arrow"
                fill
              />
            </StyledComponent.ArrowIcon>
          </CircleButtonStyled>
        </StyledComponent.IconsGroupMobileView>
      </StyledComponent.MaxWidthWrapper>
      <Image
        src="/assets/vectors/home/testimonial-section/testimonial-vector-1.svg"
        alt="vector"
        width={235}
        height={235}
        className="VectorImage"
      />
    </StyledComponent.WrapperContainer>
  );
};

const StyledComponent = {
  WrapperContainer: styled(WrapperComponent)`
        background: ${(props) => props.theme.color.testimonialBackgroundColor};

        .VectorImage {
            position: absolute;
            top: 10rem;
            left: -4rem;
            z-index: 0;
            @media (max-width: ${DeviceSize.laptop}) {
                visibility: hidden;
            }
        }
    `,
  MaxWidthWrapper: styled(MaxWidthDiv)`
        @media (min-width: ${DeviceSize.laptop}) {
            display: flex;
        }
    `,
  ImageContainer: styled(ImageWrapper)`
        @media (max-width: ${DeviceSize.laptop}) {
            display: none;
        }
    `,
  Title: styled(TitlePrimary)`
        color: ${(props) => props.theme.color.primaryDark};
    `,
  AuthorImage: styled.div`
        position: relative;
        flex: 1;
        @media (max-width: ${DeviceSize.laptop}) {
            display: none;
        }

        > img {
            border-radius: 0.938rem 0.938rem 0 0.938rem;
            object-fit: cover;
        }
    `,
  HeaderComponent: styled.div`
        display: flex;
        align-items: center;
        justify-content: space-between;
        @media (min-width: ${DeviceSize.laptop}) {
            padding-left: 2.8rem;
        }
        @media (max-width: ${DeviceSize.laptop}) {
            padding-bottom: 0;
            justify-content: center;
            margin-bottom: 9.375rem;
        }
    `,
  MainContainer: styled.div`
        width: 77%;
        display: flex;
        flex-direction: column;
        gap: 1.5rem;
        padding-top: 2rem;
        @media (max-width: ${DeviceSize.laptop}) {
            width: 100%;
            gap: 0;
            padding-top: 0;
        }
    `,
  IconsGroup: styled.div`
        display: flex;
        align-items: center;
        gap: 1.4rem;
        @media (max-width: ${DeviceSize.laptop}) {
            display: none;
        }
    `,
  ArrowIcon: styled.div`
        position: relative;
        width: 0.938rem;
        height: 0.938rem;
        > img {
            object-fit: cover;
        }
        @media (max-width: ${DeviceSize.tabletS}) {
            width: 0.625rem;
            height: 0.625rem;
        }
    `,
  IconsGroupMobileView: styled.div`
        display: none;
        @media (max-width: ${DeviceSize.laptop}) {
            display: flex;
            align-items: center;
            gap: 1.4rem;
            justify-content: center;
            margin-top: 1.25rem;
        }
    `,
};
