"use client";

import { Paragraph, TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { CircleFeature } from "../../components/molecules/home/circle-features-section/circle-feature";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { DeviceSize } from "../../const/device-size-const";
import styled from "styled-components";
import { CircleFeatureData } from "../../data/home/home.data";
import Image from "next/image";
import { FC } from "react";

interface IWhyshould {
  title?: string;
  desc?: string;
}

const ColouredWrapper = styled(WrapperComponent)`
    background-color: rgba(46, 40, 53, 0.06);

    .upper-left-vector {
        position: absolute;
    }

    .upper-right-vector {
        position: absolute;
        right: 0%;
    }

    .mid-right-vector {
        position: absolute;
        right: 0%;
        top: 40%;
    }

    .bottom-mid-vector {
        position: absolute;
        bottom: 2%;
        left: 25%;
    }

    .circle-feature-container {
        width: 100%;
        display: flex;
        justify-content: center;
        gap: 1.5rem;
        margin: 2.5rem 0px;
        align-items: center;
        &::-webkit-scrollbar {
            display: none;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            flex-direction: column;
        }

        > div {
            width: 50%;
            display: flex;
            justify-content: space-around;
            gap: 1.5rem;

            @media screen and (max-width: ${DeviceSize.tabletS}) {
                width: 100%;
            }

            @media screen and (max-width: ${DeviceSize.mobileL}) {
                align-items: center;
                flex-direction: column;
            }
        }
    }
`;

export const CircleFeatures: FC<IWhyshould> = ({ title, desc }) => {
  return (
    <ColouredWrapper>
      <MaxWidthDiv>
        <Image
          width={93}
          height={108}
          className="upper-left-vector"
          alt=""
          src="/assets/vectors/home/circle-features/circle-section-left-upper.svg"
        />
        <Image
          width={68}
          height={38}
          className="upper-right-vector"
          alt=""
          src="/assets/vectors/home/circle-features/circle-section-right-upper.svg"
        />
        <Image
          width={149}
          height={247}
          className="mid-right-vector"
          alt=""
          src="/assets/vectors/home/circle-features/circle-section-right-middle.svg"
        />
        <Image
          width={53}
          height={53}
          className="bottom-mid-vector"
          alt=""
          src="/assets/vectors/home/circle-features/circle-section-mid-bottom.svg"
        />

        <TitlePrimary color="#2c2c2c" textalign="center">
          {title}
        </TitlePrimary>
        <Paragraph margintop="0.625rem" color="#2e2835" textalign="center">
          {desc}
        </Paragraph>
        <div className="circle-feature-container">
          <div>
            <CircleFeature {...CircleFeatureData[0]} />
            <CircleFeature {...CircleFeatureData[1]} />
          </div>
          <div>
            <CircleFeature {...CircleFeatureData[2]} />
            <CircleFeature {...CircleFeatureData[3]} />
          </div>
        </div>
      </MaxWidthDiv>
    </ColouredWrapper>
  );
};
