"use client";

import styled from "styled-components";

import { Paragraph } from "../../components/atoms/title-atom";
import { ContainerButton } from "../../components/atoms/button-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { DeviceSize } from "../../const/device-size-const";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import Image from "next/image";
import { EarlyAccessSectionData } from "../../data/home/home.data";
import { StringConst } from "../../const/string-const";

const StyledComponent = {
  TopVector: styled.img`
        position: absolute;
        transform: translate(0%, -300%);
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            transform: translate(0%, -310%);
        }
    `,
  CenterDiv: styled.div`
        color: white;
        position: relative;
        margin: 50px;
        border-radius: 1.75rem;
        padding: 50px 0px;
        background-color: #8041c6;
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            font-size: 1em;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            margin: 0px auto;
            width: 100%;
            font-size: 1.2em;
        }

        > .Image1 {
            position: absolute;
            width: 17%;
            bottom: -15%;
            left: -5%;
            height: auto;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                width: 25%;
                bottom: -10%;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                display: none;
            }
        }
        > .Image2 {
            position: absolute;
            top: -12%;
            right: -7%;
            width: 13%;
            height: auto;
            border-radius: 6.25rem;
            border: 0.3125rem solid white;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                top: -8%;
                width: 18%;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                display: none;
            }
        }
        > .centerVector {
            position: absolute;
            width: 90%;
            height: 86%;
            bottom: 8%;
            left: -2%;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                display: none;
            }
        }
        > .bigHeartVector {
            position: absolute;
            top: -6%;
            right: -10%;
            width: 21.5rem;
            height: 31.875rem;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                width: 14rem;
                height: 53.438rem;
                top: 10%;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                display: none;
            }
        }
        > .leftVector {
            position: absolute;
            top: 0%;
            left: 5%;
            width: 9.8125rem;
            height: 11.625rem;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                width: 5rem;
                top: -12%;
                left: 2%;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                display: none;
            }
        }
    `,
};

const Paragraph2 = styled(Paragraph)`
    @media screen and (max-width: ${DeviceSize.tabletS}) {
        width: 80%;
        font-size: 1rem;
        line-height: 1.7rem;
    }

    @media screen and (max-width: ${DeviceSize.mobileL}) {
        font-size: 0.7rem;
        line-height: 1.5rem;
    }
`;

const HeadingDiv = styled.div`
    position: relative;
    padding: 10px 50px;
    margin-bottom: 20px;
    text-align: center;
    > h1 {
        font-size: 2rem;
        margin-bottom: 0px;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            font-size: 1.6rem;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            font-size: 1.4rem;
            margin-bottom: 10px;
        }
        @media screen and (max-width: ${DeviceSize.mobileS}) {
            font-size: 1rem;
            margin-bottom: 10px;
        }
    }
    > .smallHearts {
        position: absolute;
        top: -33%;
        left: 0%;
        width: 100%;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            width: 70%;
            top: -20%;
            left: 13%;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            top: -40%;
        }
    }
    @media screen and (max-width: ${DeviceSize.mobileL}) {
        width: 90%;
        padding: 0px 0px;
    }
`;
function EarlyAccess() {
  return (
    <WrapperComponent>
      <MaxWidthDiv>
        <StyledComponent.TopVector
          src={"/assets/vectors/home/earlyaccess-section/top-vector.svg"}
          alt=""
        />
        <StyledComponent.CenterDiv>
          <Image
            width={500}
            height={500}
            className="Image1"
            src="/assets/image/home/earlyaccess-section/left-images-group.png"
            alt={""}
          />
          <Image
            width={500}
            height={500}
            className="leftVector"
            alt=""
            src="/assets/vectors/home/earlyaccess-section/left-vectors.svg"
          />
          <Image
            width={500}
            height={500}
            className="bigHeartVector"
            alt=""
            src={
              "/assets/vectors/home/earlyaccess-section/big-heart-vector.svg"
            }
          />
          <Image
            width={100}
            height={100}
            className="centerVector"
            alt=""
            src={"/assets/vectors/home/earlyaccess-section/center-vectors.svg"}
          />
          <HeadingDiv>
            <Image
              width={650}
              height={35}
              className="smallHearts"
              src={"/assets/vectors/home/earlyaccess-section/small-hearts.svg"}
              alt=""
            />
            <h1>{EarlyAccessSectionData.heading}</h1>
          </HeadingDiv>
          <Paragraph2
            margintop={"-15px"}
            textalign={"center"}
            width={"75%"}
            fontSize={"1.1rem"}
          >
            {EarlyAccessSectionData.paragraph}
          </Paragraph2>
          <ContainerButton
            onClick={() => (window.location.href = StringConst.webLink)}
          >
            {EarlyAccessSectionData.buttonText}
          </ContainerButton>
          <Image
            width={500}
            height={500}
            className="Image2"
            src="/assets/image/home/earlyaccess-section/unsplash-1.png"
            alt={""}
          />
        </StyledComponent.CenterDiv>
      </MaxWidthDiv>
    </WrapperComponent>
  );
}

export default EarlyAccess;
