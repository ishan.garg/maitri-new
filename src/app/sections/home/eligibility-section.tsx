"use client";
import React from "react";
import styled from "styled-components";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { DeviceSize } from "../../const/device-size-const";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import Image from "next/image";
import { EligibilitySectionData } from "../../data/home/home.data";
const MaxWidthDiv2 = styled(MaxWidthDiv)`
    padding: 0px;
`;
const EligibilitySection = {
  WrapperComponent: styled(WrapperComponent)`
        padding: 10rem 0rem;
    `,
  MainContainer: styled.div`
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            flex-direction: column;
            align-items: center;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            width: 100%;
            margin: 0rem;
        }
        > .image {
            width: 33%;
            z-index: 1;
            height: 35%;
            object-fit: cover;
            z-index: 2;
            margin-top: -10rem;
            margin-right: -7rem;

            border-radius: 1rem;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                width: 90%;
                height: 70%;
                margin-right: 0rem;
                margin-bottom: -4rem;
                margin-top: 0px;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                object-fit: cover;
            }
        }
        > .icon1 {
            position: absolute;
            width: 602px;
            height: 407px;
            bottom: 50%;
            right: 60%;
            z-index: 0;
        }
    `,

  ContentBox: styled.div`
        z-index: 1;
        width: 100%;
        border-radius: 0px 0px 0px 20px;
        background-color: #8041c6;
        display: flex;
        justify-content: flex-start;
        position: relative;
        overflow: hidden;
        align-items: center;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            flex-direction: column;
            justify-content: flex-end;
            align-items: center;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            border-radius: 0px 0px 0px 0px;
        }
        > .contentBox {
            margin-bottom: 5rem;
            margin-top: 30px;
            margin-left: 10rem;
            display: flex;
            justify-content: space-evenly;
            align-items: flex-start;
            flex-direction: column;

            @media screen and (max-width: ${DeviceSize.tabletS}) {
                justify-content: flex-end;
                font-size: 1.1em;
                -webkit-box-align: center;
                align-items: flex-start;
                border-radius: 1.875rem;
                padding: 2.25rem;
                margin-left: 0rem;
                margin-top: 8rem;
                margin-bottom: 0rem;
                width: 100%;
            }

            @media screen and (max-width: ${DeviceSize.mobileS}) {
                font-size: 1em;
                margin-top: 4rem;
                padding: 1.25rem;
            }

            > h2 {
                text-align: left;
                color: white;
                width: 75%;
                font-weight: 700;
                font-size: 2.3rem;
                line-height: 2.85rem;
                @media screen and (max-width: ${DeviceSize.tabletS}) {
                    text-align: left;
                    line-height: 2.5rem;
                    font-size: 2rem;
                    width: 100%;
                    margin-bottom: 1rem;
                }

                @media screen and (max-width: ${DeviceSize.mobileL}) {
                    font-size: 1.5rem;
                    width: 100%;
                }
            }

            > .buttonDiv {
                width: 100%;
                margin-top: 2rem;

                > button {
                    border-radius: 80px;
                    border: none;
                    color: #8041c6;
                    background-color: white;
                    width: 21.688rem;
                    height: 3.125rem;
                    font-weight: 600;
                    font-size: 1.125rem;
                    @media screen and (max-width: ${DeviceSize.tabletS}) {
                        width: 17.5rem;
                        font-size: 1.125rem;
                    }
                    @media screen and (max-width: ${DeviceSize.mobileL}) {
                        height: 2.813rem;
                        padding: 0px;
                        width: 11.25rem;
                        font-size: 0.75rem;
                    }
                    @media screen and (max-width: ${DeviceSize.mobileS}) {
                        height: 2.5rem;
                        width: 10.625rem;
                        font-size: 0.625rem;
                    }
                }
                @media screen and (max-width: ${DeviceSize.tabletS}) {
                    text-align: center;
                }
            }
        }
        > .icon {
            position: absolute;
            right: -2%;
            top: 10%;
            @media screen and (max-width: ${DeviceSize.mobileS}) {
                display: none;
            }
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                right: -4%;
            }
        }
    `,
  List: styled.ul`
        text-align: justify;
        list-style: none;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: flex-start;
        margin-top: 0.625rem;
        margin-right: 1.5rem;
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            justify-content: center;
            margin-top: 0rem;
            font-size: 0.8em;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            width: 100%;
            align-items: flex-start;
        }
        > li {
            text-align: left;
            margin-bottom: 0.75rem;
            display: flex;
            align-items: center;
            color: white;
            > span {
                margin-top: 10px;
                line-height: 1.4rem;
            }
            > .icon4 {
                margin-top: 10px;
                margin-right: 10px;
                width: 1.25rem;
                height: 1.188rem;
                @media screen and (max-width: ${DeviceSize.mobileS}) {
                    width: 0.938rem;
                    font-size: 0.625rem;
                }
            }
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                margin-bottom: 0.75rem;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                margin-top: 0.625rem;
            }
            @media screen and (max-width: ${DeviceSize.mobileS}) {
                font-size: 0.8rem;
            }
        }
    `,
};

function Eligibility() {
  return (
    <EligibilitySection.WrapperComponent>
      <MaxWidthDiv2>
        <EligibilitySection.MainContainer>
          <Image
            width={500}
            height={500}
            className="image"
            src="/assets/image/home/eligibility-section/wedding-image.png"
            alt={"Not Found"}
          />
          <Image
            width={500}
            height={500}
            className="icon1"
            alt=""
            src={"/assets/vectors/home/eligibility-section/icon3.svg"}
          />
          <EligibilitySection.ContentBox>
            <div className="contentBox">
              <h2>{EligibilitySectionData.heading}</h2>
              <EligibilitySection.List>
                {EligibilitySectionData.list.map((item, index) => (
                  <li key={index}>
                    <Image
                      width={500}
                      height={500}
                      className="icon4"
                      alt=""
                      src={"/assets/vectors/home/eligibility-section/icon4.svg"}
                    />
                    <span>{item.text}</span>
                  </li>
                ))}
              </EligibilitySection.List>
              <div className="buttonDiv">
                <button>{EligibilitySectionData.buttonText}</button>
              </div>
            </div>
            <Image
              width={800}
              height={500}
              className="icon"
              alt=""
              src={"/assets/vectors/home/eligibility-section/icon.svg"}
            />
          </EligibilitySection.ContentBox>
        </EligibilitySection.MainContainer>
      </MaxWidthDiv2>
    </EligibilitySection.WrapperComponent>
  );
}

export default Eligibility;
