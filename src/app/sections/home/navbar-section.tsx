"use client";

import { DeviceSize } from "../../const/device-size-const";
import React, { useState } from "react";
import styled from "styled-components";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { NavTitles } from "../../data/home/home.data";
import Image from "next/image";

const NavBar = {
  Container: styled.div`
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        position: sticky;
        top: 0;
        z-index: 1000;
        > button {
            display: none;
            background: none;
            border: none;
        }

        @media screen and (max-width: ${DeviceSize.tabletS}) {
            background-color: #8041c6;
            color: white;

            > button {
                margin-left: 20px;
                display: block;
            }
        }
    `,
  LogoDiv: styled.div`
        margin: 1.5rem 0;
        display: flex;
        justify-content: center;
        align-items: center;
        > .logo {
            margin-right: 1rem;
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                width: 10%;
                height: 10%;
            }
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            width: 90%;
            text-align: center;
        }
        > span {
            font-size: 2.2rem;
            color: #8041c6;
            font-weight: 700;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                color: white;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                font-size: 1.3em;
            }
        }
    `,

  NavList: styled.ul`
        color: black;
        display: flex;
        list-style: none;
        justify-content: space-evenly;
        font-weight: 650;
        font-size: 1.225rem;
        width: 55%;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            display: none;
        }
        > a {
            cursor: pointer;
            &:hover {
                color: #8041c6;
                text-decoration: underline;
            }
        }
    `,
  Slider: styled.img`
        height: 1.25rem;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            margin-left: 0.625rem;
        }
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            display: block;
        }
    `,
  SliderContainer: styled.div`
        position: absolute;
        z-index: 200000;
        background-color: #8041c6;
        padding: 0.625rem;
        height: 12.5rem;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;

        > div {
            height: 80%;
            width: 90%;
            display: flex;
            justify-content: space-evenly;
            flex-direction: column;
            align-items: center;
            > a {
                &:hover {
                    color: black;
                }
            }
        }
    `,
  CloseTag: styled.img`
        position: absolute;
        top: 20%;
        left: 1%;
        height: 15%;
        width: 10%;
    `,
};
const MaxWidthDivNav = styled(MaxWidthDiv)`
    @media screen and (max-width: ${DeviceSize.tabletS}) {
        padding: 0px;
    }
`;
function Navbar() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      {isOpen && (
        <NavBar.SliderContainer>
          <button onClick={() => setIsOpen(!isOpen)}>
            <NavBar.CloseTag
              alt="Menu"
              src={"/assets/vectors/home/navbar/cross-sign.svg"}
            />
          </button>

          <div>
            {NavTitles.map((item, index) => (
              <a key={index} href={item.href}>
                {item.title}
              </a>
            ))}
          </div>
        </NavBar.SliderContainer>
      )}
      <MaxWidthDivNav>
        <NavBar.Container>
          <button onClick={() => setIsOpen(!isOpen)}>
            <NavBar.Slider
              alt=""
              src={"/assets/vectors/home/navbar/navbar-slider.svg"}
            />
          </button>
          <NavBar.LogoDiv>
            <Image
              className="logo"
              width={50}
              height={50}
              src="/logo.ico"
              alt={""}
            />
            <span>Matrimilan</span>
          </NavBar.LogoDiv>
          <NavBar.NavList>
            {NavTitles.map((item, index) => (
              <a
                key={index}
                href={item.href}
                className={item.title === "Early Access" ? "special" : ""}
              >
                {item.title}
              </a>
            ))}
          </NavBar.NavList>
        </NavBar.Container>
      </MaxWidthDivNav>
    </>
  );
}

export default Navbar;
