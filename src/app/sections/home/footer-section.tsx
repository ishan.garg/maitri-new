"use client";

import { Paragraph, TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { CircleButtonStyled } from "../../components/atoms/button-atom";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { FC } from "react";
import styled from "styled-components";
import { FooterSectionData } from "../../data/home/home.data";
import { DeviceSize } from "../../const/device-size-const";
import Image from "next/image";

export const FooterSection: FC = () => {
  return (
    <StyledComponent.StyledWrapper>
      <StyledComponent.MaxWidthWrapper>
        <TitlePrimary color="white">{FooterSectionData.title}</TitlePrimary>
        <StyledComponent.ParagraphWrapper color="white">
          {FooterSectionData.subtitle}
        </StyledComponent.ParagraphWrapper>
        <div>
          <CircleButtonStyled background="rgba(255,255,255,0.1)">
            <StyledComponent.SocialIcon>
              <Image
                src="/assets/icons/home/footer-section/facebook.svg"
                alt="facebook"
                fill
              />
            </StyledComponent.SocialIcon>
          </CircleButtonStyled>
          <CircleButtonStyled background="rgba(255,255,255,0.1)">
            <StyledComponent.SocialIcon>
              <Image
                src="/assets/icons/home/footer-section/youtube.svg"
                alt="youtube"
                fill
              />
            </StyledComponent.SocialIcon>
          </CircleButtonStyled>
          <CircleButtonStyled background="rgba(255,255,255,0.1)">
            <StyledComponent.SocialIcon>
              <Image
                src="/assets/icons/home/footer-section/twitter.svg"
                alt="twitter"
                fill
              />
            </StyledComponent.SocialIcon>
          </CircleButtonStyled>
          <CircleButtonStyled background="rgba(255,255,255,0.1)">
            <StyledComponent.SocialIcon>
              <Image
                src="/assets/icons/home/footer-section/instagram.svg"
                alt="instagram"
                fill
              />
            </StyledComponent.SocialIcon>
          </CircleButtonStyled>
        </div>
        <div className="Vector1">
          <Image
            src="/assets/vectors/home/footer-section/footer-vector-1.svg"
            alt="vector1"
            fill
          />
        </div>
        <div className="Vector2">
          <Image
            src="/assets/vectors/home/footer-section/footer-vector-2.svg"
            alt="vector2"
            fill
          />
        </div>
        <div className="Vector3">
          <Image
            src="/assets/vectors/home/footer-section/footer-vector-3.svg"
            alt="vector3"
            fill
          />
        </div>
        <div className="Vector4">
          <Image
            src="/assets/vectors/home/footer-section/footer-vector-4.svg"
            alt="vector4"
            fill
          />
        </div>
      </StyledComponent.MaxWidthWrapper>
    </StyledComponent.StyledWrapper>
  );
};

const StyledComponent = {
  StyledWrapper: styled(WrapperComponent)`
        background: ${(props) => props.theme.color.purpleColor};
        margin-top: 3rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            margin-top: 1rem;
        }
    `,
  MaxWidthWrapper: styled(MaxWidthDiv)`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 0.7rem;

        > div {
            display: flex;
            align-items: center;
            gap: 0.8rem;
            margin-top: 1rem;
            @media (max-width: ${DeviceSize.tabletS}) {
                margin-top: 0.3rem;
            }
        }

        .Vector1 {
            position: absolute;
            width: 5.625rem;
            height: 5.625rem;
            left: 0;
            bottom: 0;

            @media (max-width: ${DeviceSize.tabletS}) {
                width: 2.5rem;
                height: 2.5rem;
            }
        }
        .Vector2 {
            position: absolute;
            width: 4.375rem;
            height: 4.375rem;
            left: 15%;
            top: 30%;

            @media (max-width: ${DeviceSize.tabletS}) {
                width: 2.5rem;
                height: 2.5rem;
            }
        }
        .Vector3 {
            position: absolute;
            width: 12.5rem;
            height: 7.5rem;
            right: 17.5vw;
            bottom: 0;

            @media (max-width: ${DeviceSize.tabletS}) {
                width: 2.5rem;
                height: 2.5rem;
            }
        }
        .Vector4 {
            position: absolute;
            width: 12.5rem;
            height: 12.5rem;
            right: 0;
            bottom: 0;

            @media (max-width: ${DeviceSize.tabletS}) {
                width: 5rem;
                height: 5rem;
            }
        }
    `,
  ParagraphWrapper: styled(Paragraph)`
        text-align: center;
        width: 80%;
    `,
  SocialIcon: styled.div`
        position: relative;
        width: 1.1rem;
        height: 1.1rem;
        @media (max-width: ${DeviceSize.tabletS}) {
            width: 0.75rem;
            height: 0.75rem;
        }
    `,
};
