"use client";
import { Paragraph, TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { StatsCard } from "../../components/molecules/home/community-section/stats-card.comp";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { FC } from "react";
import styled from "styled-components";
import { CommunitySectionData } from "../../data/home/home.data";
import { DeviceSize } from "../../const/device-size-const";
import Image from "next/image";

export const Community: FC = () => {
  return (
    <WrapperComponent>
      <StyledComponent.MaxWidthWrapper>
        <StyledComponent.Title>
          {CommunitySectionData.title}
        </StyledComponent.Title>
        <StyledComponent.SubTitle>
          {CommunitySectionData.subtitle1}
        </StyledComponent.SubTitle>
        <StyledComponent.SubTitle>
          {CommunitySectionData.subtitle2}
        </StyledComponent.SubTitle>
        <StyledComponent.MapContainer>
          <Image
            src="/assets/vectors/home/community-section/world.svg"
            alt="world"
            fill
          />
          <StyledComponent.Card1>
            <div style={{ position: "relative" }}>
              <StatsCard
                count={CommunitySectionData.stats[0].count}
                title={CommunitySectionData.stats[0].title}
                users={CommunitySectionData.stats[0].users}
              />
              <Image
                src="/assets/vectors/home/community-section/earth-vector1.svg"
                alt="vector"
                width={80}
                height={80}
                className="Vector1"
              />
            </div>
          </StyledComponent.Card1>
          <StyledComponent.Card2>
            <div style={{ position: "relative" }}>
              <StatsCard
                count={CommunitySectionData.stats[1].count}
                title={CommunitySectionData.stats[1].title}
                users={CommunitySectionData.stats[1].users}
              />
              <Image
                src="/assets/vectors/home/community-section/earth-vector2.svg"
                alt="vector"
                width={20}
                height={20}
                className="Vector2"
              />
            </div>
          </StyledComponent.Card2>
          <StyledComponent.Card3>
            <div style={{ position: "relative" }}>
              <StatsCard
                count={CommunitySectionData.stats[2].count}
                title={CommunitySectionData.stats[2].title}
                users={CommunitySectionData.stats[2].users}
              />
              <Image
                src="/assets/vectors/home/community-section/earth-vector3.svg"
                alt="vector"
                width={90}
                height={90}
                className="Vector3"
              />
            </div>
          </StyledComponent.Card3>
        </StyledComponent.MapContainer>
        <StyledComponent.StatsContainerMobile>
          {CommunitySectionData.stats.map((item, index) => {
            return <StatsCard key={index} {...item} />;
          })}
        </StyledComponent.StatsContainerMobile>
      </StyledComponent.MaxWidthWrapper>
    </WrapperComponent>
  );
};

const StyledComponent = {
  MaxWidthWrapper: styled(MaxWidthDiv)`
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;
    `,
  Title: styled(TitlePrimary)`
        text-align: center;
      
        @media (max-width: ${DeviceSize.laptop}) {
            width: 70%;
        }
    `,
  SubTitle: styled(Paragraph)`
        width: 60%;
        text-align: center;
        @media (max-width: ${DeviceSize.laptop}) {
            width: 90%;
        }
      
    `,
  MapContainer: styled.div`
        position: relative;
        width: 65%;
        height: 25rem;
        margin-top: 2.6rem;

        @media (max-width: ${DeviceSize.laptop}) {
            display: none;
        }

        .Vector1 {
            position: absolute;
            top: -3.125rem;
            right: -1.875rem;
        }
        .Vector2 {
            position: absolute;
            bottom: 2.5rem;
            left: -1.875rem;
        }
        .Vector3 {
            position: absolute;
            top: -5.625rem;
            right: 1.563rem;
        }
    `,
  StatsContainerMobile: styled.div`
        display: none;
        @media (max-width: ${DeviceSize.laptop}) {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            gap: 1.25rem;
        }
    `,
  Card1: styled.div`
        position: absolute;
        top: 35%;
        left: -2%;
    `,
  Card2: styled.div`
        position: absolute;
        top: 10%;
        right: 10%;
    `,
  Card3: styled.div`
        position: absolute;
        bottom: 0;
        right: 27%;
    `,
};

//color property deleted
