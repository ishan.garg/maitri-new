"use client";

import { Paragraph, TitlePrimary } from "../../components/atoms/title-atom";
import { WrapperComponent } from "../../components/atoms/wrapper-atom";
import { DeviceSize } from "../../const/device-size-const";
import { HeroSectionData } from "../../data/home/home.data";
import { MaxWidthDiv } from "../../theme/matri-default.theme";
import { FC } from "react";
import styled from "styled-components";
import { StringConst } from "../../const/string-const";

export const HeroSection: FC = () => {
  return (
    <HeroComponent.WrapperComponent>
      <MaxWidthDiv>
        <HeroComponent.Content>
          <HeroComponent.TitlePrimary2>
            {HeroSectionData.title}
          </HeroComponent.TitlePrimary2>
          <HeroComponent.Paragraph2>
            {HeroSectionData.description}
          </HeroComponent.Paragraph2>
          <h4>{HeroSectionData.subHeading}</h4>
          <HeroComponent.ButtonContainer>
            <button
              onClick={() => (window.location.href = StringConst.webLink)}
            >
              Bride
            </button>
            <button
              onClick={() => (window.location.href = StringConst.webLink)}
            >
              Groom
            </button>
          </HeroComponent.ButtonContainer>
        </HeroComponent.Content>
      </MaxWidthDiv>
    </HeroComponent.WrapperComponent>
  );
};

const HeroComponent: any = {
  WrapperComponent: styled(WrapperComponent)`
        background: linear-gradient(90.44deg, rgba(0, 0, 0, 0.7) 0.36%, rgba(0, 0, 0, 0) 99.62%), url('/assets/image/home/hero-section/lading-image.png');
        aspect-ratio: 2/1;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        height: 54rem;
        max-height: 60rem;

        @media screen and (max-width: ${DeviceSize.tabletS}) {
            height: 30rem;
            background: linear-gradient(90.44deg, rgba(0, 0, 0, 0.7) 0.36%, rgba(0, 0, 0, 0) 99.62%), url('/assets/image/home/hero-section/mobile-image.png');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            height: 22rem;
        }
        @media screen and (max-width: ${DeviceSize.mobileS}) {
            height: 22rem;
        }
    `,

  Content: styled.div`
        margin-top: 9rem;
        border-radius: 25px;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        color: white;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            margin-top: 0px;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
        }

        > h4 {
            display: block;
            text-align: left;
            margin-top: 1.8rem;
            font-weight: 600;
            font-size: 1.4rem;
            @media screen and (max-width: ${DeviceSize.tabletS}) {
                font-size: 1.4rem;
                line-height: 1.3rem;
            }
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                margin-top: 1.5rem;
                font-size: 1.2rem;
                margin-bottom: 0rem;
            }
        }
    `,

  TitlePrimary2: styled(TitlePrimary)`
        font-size: 4.125rem;
        max-width: 560px;
        line-height: 5.2rem;
        margin-bottom: 28px;
        font-weight: 600;

        @media screen and (max-width: ${DeviceSize.tabletS}) {
            font-size: 3rem;
            line-height: 1.2;
            margin-bottom: 1.5rem;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            font-size: 1.75rem;
            line-height: 2.4rem;
            width: 100%;
            margin-bottom: 1.95rem;
        }
    `,
  Paragraph2: styled(Paragraph)`
        width: 100%;
        font-size: 1.4rem;
        font-weight: 300;
        line-height: 2.2rem;
        max-width: 740px;
        margin-top: -0.91rem;
        margin-bottom: 1.5rem;
        @media screen and (max-width: ${DeviceSize.tabletS}) {
            width: 90%;
            font-size: 1.25rem;
            margin-bottom: 1rem;
            line-height: 2.35rem;
        }
        @media screen and (max-width: ${DeviceSize.mobileL}) {
            margin-bottom: 0rem;
            font-size: 0.875rem;
            width: 100%;
            line-height: 1.55rem;
        }
    `,
  ButtonContainer: styled.div`
        width: 316px;
        display: flex;
        flex-direction: row;
        margin: 15px 0 30px;
        gap: 20px;
        @media screen and (max-width: ${DeviceSize.mobileL}) {
        }
        > button {
            padding: 20px 40px;
            cursor: pointer;
            font-weight: 600;
            font-size: 1rem;
            border-radius: 80px;
            background: transparent;
            color: white;
            border: 2px solid white;
            @media screen and (max-width: ${DeviceSize.mobileL}) {
                padding: 15px 30px;
                font-size: 0.75rem;
            }
        }
    `,
};
