/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    images: {
        domains: ['blog.matrimilan.com', 'knotsbyamp.com', 'media.istockphoto.com', 'images.unsplash.com', 'images.pexels.com']
    }
};

module.exports = nextConfig;
